$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
});
$(".carousel").carousel({
  interval: 4000,
});

$("#pagar").on("show.bs.modal", function (e) {
  console.log("se va a mostrar el modal");
  $("#pagarBtn").removeClass("btn-primary");
  $("#pagarBtn").addClass("btn-secondary");
  $("#pagarBtn").prop("disabled", true);
  $("#pagarBtn1").removeClass("btn-primary");
  $("#pagarBtn1").addClass("btn-secondary");
  $("#pagarBtn1").prop("disabled", true);
  $("#pagarBtn2").removeClass("btn-primary");
  $("#pagarBtn2").addClass("btn-secondary");
  $("#pagarBtn2").prop("disabled", true);
});
$("#pagar").on("shown.bs.modal", function (e) {
  console.log("se mostró el modal");
});
$("#pagar").on("hide.bs.modal", function (e) {
  console.log("se va a ocultar el modal");
});
$("#pagar").on("hidden.bs.modal", function (e) {
  console.log("se va a ocultó el modal");
  $("#pagarBtn").prop("disabled", false);
  $("#pagarBtn").removeClass("btn-secondary");
  $("#pagarBtn").addClass("btn-primary");

  $("#pagarBtn1").prop("disabled", false);
  $("#pagarBtn1").removeClass("btn-secondary");
  $("#pagarBtn1").addClass("btn-primary");

  $("#pagarBtn2").prop("disabled", false);
  $("#pagarBtn2").removeClass("btn-secondary");
  $("#pagarBtn2").addClass("btn-primary");
});
